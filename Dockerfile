
# base image
FROM node:13.7

# set working directory
WORKDIR /usr/src/app

# concat multiple run instructions
# sort them alphabetically
RUN yarn install

COPY . .

EXPOSE 3000

CMD ["yarn", "start"]